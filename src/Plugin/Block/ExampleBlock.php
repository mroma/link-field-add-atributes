<?php

namespace Drupal\link_field_add_atributes\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "link_field_add_atributes_example",
 *   admin_label = @Translation("Example"),
 *   category = @Translation("link field add atributes")
 * )
 */
class ExampleBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['content'] = [
      '#markup' => $this->t('It works!'),
    ];
    return $build;
  }

}
