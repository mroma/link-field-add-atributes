<?php

namespace Drupal\link_field_add_atributes\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for link field add atributes routes.
 */
class LinkFieldAddAtributesController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
